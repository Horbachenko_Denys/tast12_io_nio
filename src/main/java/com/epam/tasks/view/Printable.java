package com.epam.tasks.view;

public interface Printable {

    void print();
}
