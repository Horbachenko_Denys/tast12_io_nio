package com.epam.tasks.view;

import com.epam.tasks.controller.Controller;
import com.epam.tasks.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    public static Logger logger = LogManager.getLogger(MyView.class);
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        setMenu();
        this.methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - create ship with droids, serialize and deserialize...");
        menu.put("2", " 2 - read and write with usual and buffered readers and writers...");
        menu.put("3", " 3 - InputStream, push read data to stream...");
        menu.put("4", " 4 - read Java source file ...");
        menu.put("5", " 5 - display contents of specific directory...");
        menu.put("6", " 6 - read and write using SomeBuffer(Java NIO)...");
        menu.put("Q", " Q - Quit");
    }

    private void pressButton1() {
        controller.createDroidShip();
    }

    private void pressButton2() { controller.readByUsualAndBufferedReader(); }

    private void pressButton3() {
        controller.pushDataToStream();
    }

    private void pressButton4() { controller.readJavaFile(); }

    private void pressButton5() { controller.displayContentOfDirectory(); }

    private void pressButton6() { controller.readAndWriteByNIO(); }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error in menu");
            }
        } while (!keyMenu.equals("Q"));
    }
}
