package com.epam.tasks.model;


public interface Model {

    void createDroidShip();

    void readByUsualAndBufferedReader();

    void pushDataToStream();

    void readJavaFile();

    void displayContentOfDirectory();

    void readAndWriteByNIO();
}
