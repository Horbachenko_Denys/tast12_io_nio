package com.epam.tasks.model;

import com.epam.tasks.model.droid.Ship;
import com.epam.tasks.model.reader.MyReader;

import java.io.File;
import java.io.IOException;

public class ModelImpl implements Model {
    private Ship ship;
    private MyReader myReader;
    private MyInputStream myInputStream;
    private JavaFileReader javaFileReader;

    @Override
    public void createDroidShip() {
        ship = new Ship("Enterprise","Cruiser");
        ship.fillShip();
        ship.serializeShip();
        ship.deserializeShip();
    }

    @Override
    public void readByUsualAndBufferedReader() {
        myReader = new MyReader();
        myReader.compareReaderPerformance("./text2MB.txt");
        myReader.compareWriterPerformance("./text2MB.txt");
        myReader.compareReaderPerformance("./text5MB.txt");
        myReader.compareWriterPerformance("./text5MB.txt");
        myReader.compareReaderPerformance("./text10MB.txt");
        myReader.compareWriterPerformance("./text10MB.txt");
        myReader.compareReaderPerformance("./text15MB.txt");
        myReader.compareWriterPerformance("./text15MB.txt");
    }

    @Override
    public void pushDataToStream() {
        myInputStream = new MyInputStream();
        myInputStream.createMyInputStream();
    }

    @Override
    public void readJavaFile() {
        javaFileReader.findComment("Enter file path");
    }

    @Override
    public void displayContentOfDirectory() {
        System.out.println("Contents of specific directory");
        File file = new File("");
        if (file.exists()) {
            printDirectory(file,"");
        } else
            System.out.println("Directory do not exist");
    }

    private void printDirectory (File file, String str) {
        System.out.println(str + "Directory: " + file.getName());
        str = str + " ";
        File[] fileNames = file.listFiles();
        for (File f : fileNames) {
            if (f. isDirectory()) {
                printDirectory(f, str);
            } else {
                System.out.println(str + "File: " + f.getName());
            }
        }
    }

    @Override
    public void readAndWriteByNIO() {
        SomeBuffer someBuffer = new SomeBuffer();
            someBuffer.read();
            someBuffer.write();
    }
}