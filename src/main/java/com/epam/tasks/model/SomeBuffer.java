package com.epam.tasks.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class SomeBuffer {
    RandomAccessFile file;
    FileChannel channel;
    ByteBuffer byteBuffer;
    int bytesRead;
    int bytesWrite;

    SomeBuffer () {
        try {
            file = new RandomAccessFile("text2MB.txt","rw");
            channel = file.getChannel();
            byteBuffer = ByteBuffer.allocate(24);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    void read() {
        try {
            bytesRead = channel.read(byteBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byteBuffer.flip();
    }

    void write() {
        try {
            bytesWrite = channel.write(byteBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
