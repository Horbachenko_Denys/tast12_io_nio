package com.epam.tasks.model.droid;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    private int id;
    private transient int strength;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public Droid(String name, int id, int strength) {
        this.name = name;
        this.id = id;
        this.strength = strength;
    }

}
